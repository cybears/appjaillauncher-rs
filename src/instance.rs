use appcontainer;
use std::io::{Error, ErrorKind};
use std::path::{Path, PathBuf};
use utils::{AclEntry, HandlePtr, WindowsProcess};
use winapi::um::winnt::{GENERIC_EXECUTE, GENERIC_READ};

pub struct Instance {
    pub profile: appcontainer::Profile,
    pub acls: Vec<AclEntry>,
    pub process: WindowsProcess,
}

pub fn start_instance(
    profile_name: &str,
    outbound: bool,
    debug: bool,
    key_path: &PathBuf,
    child_path: &Path,
    in_handle: HandlePtr,
    out_handle: HandlePtr,
) -> Result<Instance, Error> {
    // Create profile
    let mut profile = appcontainer::Profile::new(profile_name).map_err(|e| {
        Error::new(
            ErrorKind::Other,
            format!(
                "Failed to create AppContainer profile for {:}: error={:}",
                profile_name, e
            ),
        )
    })?;

    info!("sid = {:}", profile.sid);

    // Configure settings
    profile.enable_outbound_network(outbound);
    info!("AppContainer.enable_outbound_network_conn = {:}", outbound);

    profile.enable_debug(debug);
    info!("AppContainer.enable_debug = {:}", debug);

    // Add ACLs
    let mut key_dir_path = key_path.clone();
    key_dir_path.pop();
    let mut acls: Vec<AclEntry> = Vec::new();

    info!(
        "Adding AppContainer profile ACL entry into {:?}",
        key_dir_path
    );
    acls.push(AclEntry::new(
        &key_dir_path,
        &profile.sid,
        GENERIC_READ | GENERIC_EXECUTE,
        false,
    )?);

    info!("Adding AppContainer profile ACL entry into {:?}", key_path);
    acls.push(AclEntry::new(&key_path, &profile.sid, GENERIC_READ, false)?);

    // Get current directory for child process. Canonicalize seems to give a UNC path
    // which does not play nicely with some applications, so as a hack, strip the \\?\
    // from the start if it's there.
    let key_dir_canonical = key_dir_path.canonicalize().unwrap();
    let mut key_dir_abspath = key_dir_canonical.to_str().unwrap();
    if key_dir_abspath.starts_with("\\\\?\\") {
        key_dir_abspath = &key_dir_abspath[4..];
    }
    info!("key_dir_abspath = {:?}", key_dir_abspath);

    let process = profile
        .launch(
            in_handle,
            out_handle,
            key_dir_abspath,
            child_path.to_str().unwrap(),
        )
        .map_err(|e| {
            Error::new(
                ErrorKind::Other,
                format!("Failed to launch new process: error={:}", e),
            )
        })?;

    info!(
        "Launched new process with handle {:?} and current_dir = {:?}",
        process.handle.raw, key_dir_path
    );

    Ok(Instance {
        profile,
        acls,
        process,
    })
}
