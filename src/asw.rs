#![cfg(windows)]

#[allow(unused_imports)]
use log::*;

use libc;
use std::collections::HashMap;
use std::ffi::OsStr;
use std::iter::once;
use std::mem;
use std::os::windows::ffi::OsStrExt;
use widestring::WideString;

use winapi::shared::inaddr::in_addr;
use winapi::shared::minwindef::{DWORD, INT, LPVOID};
use winapi::shared::ntdef::NULL;
use winapi::shared::ws2def::{
    ADDRINFOW, AF_INET, AI_PASSIVE, SOCKADDR, SOCKADDR_IN, SOCK_STREAM, SOL_SOCKET, SO_REUSEADDR,
};
use winapi::um::errhandlingapi::GetLastError;
use winapi::um::handleapi::SetHandleInformation;
use winapi::um::minwinbase::LPSECURITY_ATTRIBUTES;
use winapi::um::synchapi::CreateEventW;
use winapi::um::winbase::HANDLE_FLAG_INHERIT;
use winapi::um::winnt::{HANDLE, LPCWSTR, PCWSTR};
use winapi::um::winsock2::{
    accept, bind, closesocket, listen, setsockopt, WSACleanup, WSAEventSelect, WSAGetLastError,
    WSASocketW, WSAStartup, WSAWaitForMultipleEvents, FD_ACCEPT, INVALID_SOCKET, SOCKET,
    SOCKET_ERROR, WSADATA, WSAPROTOCOL_INFOW,
};
use winapi::um::ws2tcpip::{GetAddrInfoW, InetNtopW};

#[derive(Hash, Eq, PartialEq, Clone)]
pub enum TcpServerEvent {
    Accept,
    Error,
    Token(String),
}

pub struct TcpClient {
    socket: SOCKET,
}

pub trait HasRawHandle {
    fn raw_handle(&self) -> HANDLE;
}

impl TcpClient {
    fn from_accept(socket: SOCKET) -> TcpClient {
        TcpClient { socket: socket }
    }
}

impl HasRawHandle for TcpClient {
    fn raw_handle(&self) -> HANDLE {
        self.socket as HANDLE
    }
}

impl Drop for TcpClient {
    fn drop(&mut self) {
        unsafe {
            closesocket(self.socket);
        }
    }
}

pub struct TcpServer {
    socket: SOCKET,
    event_list: HashMap<TcpServerEvent, HANDLE>,
    handle_accept: HANDLE,
}

impl TcpServer {
    pub fn bind(port: &str) -> Result<TcpServer, DWORD> {
        let mut data: WSADATA = unsafe { mem::zeroed() };
        let mut ret = unsafe { WSAStartup(0x202, &mut data) };
        if ret != 0 {
            return Err(ret as DWORD);
        }

        let wsz_port: Vec<u16> = OsStr::new(port).encode_wide().chain(once(0)).collect();

        let mut hints: ADDRINFOW = unsafe { mem::zeroed() };
        let mut servinfo: *mut ADDRINFOW = NULL as *mut ADDRINFOW;

        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE;

        ret = unsafe { GetAddrInfoW(0 as PCWSTR, wsz_port.as_ptr(), &hints, &mut servinfo) };
        if ret != 0 {
            debug!("GetAddrInfoW failed: GLE={:}", unsafe { WSAGetLastError() });
            return Err(ret as DWORD);
        }

        let socket = unsafe {
            WSASocketW(
                (*servinfo).ai_family,
                (*servinfo).ai_socktype,
                (*servinfo).ai_protocol,
                mem::transmute::<usize, *mut WSAPROTOCOL_INFOW>(0),
                0,
                0,
            )
        };
        if socket == INVALID_SOCKET {
            debug!("WSASocketW failed: GLE={:}", unsafe { WSAGetLastError() });
            return Err(unsafe { WSAGetLastError() as DWORD });
        }

        let mut server = TcpServer {
            socket: socket,
            event_list: HashMap::new(),
            handle_accept: unsafe {
                CreateEventW(0 as LPSECURITY_ATTRIBUTES, 0, 0, NULL as LPCWSTR)
            },
        };
        if server.handle_accept == NULL {
            debug!("CreateEventW failed: GLE={:}", unsafe { GetLastError() });
            return Err(unsafe { GetLastError() } as DWORD);
        }

        server
            .event_list
            .insert(TcpServerEvent::Accept, server.handle_accept);

        let yes: DWORD = 1;
        ret = unsafe {
            setsockopt(
                socket,
                SOL_SOCKET,
                SO_REUSEADDR,
                mem::transmute::<&DWORD, *const i8>(&yes),
                mem::size_of::<DWORD>() as i32,
            )
        };
        if ret != 0 {
            debug!("setsockopt SO_REUSEADDR failed: GLE={:}", unsafe {
                WSAGetLastError()
            });
            return Err(ret as DWORD);
        }

        ret = unsafe { bind(socket, (*servinfo).ai_addr, (*servinfo).ai_addrlen as i32) };
        if ret == SOCKET_ERROR {
            debug!("bind failed: GLE={:}", unsafe { WSAGetLastError() });
            return Err(unsafe { WSAGetLastError() } as DWORD);
        }

        ret = unsafe { listen(socket, 5) };
        if ret == SOCKET_ERROR  {
            debug!("listen failed: GLE={:}", unsafe { WSAGetLastError() });
            return Err(unsafe { WSAGetLastError() } as DWORD);
        }

        ret = unsafe { WSAEventSelect(socket, server.handle_accept, FD_ACCEPT) };
        if ret == SOCKET_ERROR {
            debug!("WSAEventSelect failed: GLE={:}", unsafe {
                WSAGetLastError()
            });
            return Err(unsafe { WSAGetLastError() } as DWORD);
        }

        if unsafe { SetHandleInformation(socket as HANDLE, HANDLE_FLAG_INHERIT, 0) } == 0 {
            debug!("Failed to SetHandleInformation: GLE={:}", unsafe {
                GetLastError()
            });
            return Err(unsafe { GetLastError() });
        }

        Ok(server)
    }

    pub fn register_event(&mut self, name: String, event: HANDLE) {
        self.event_list.insert(TcpServerEvent::Token(name), event);
    }

    pub fn deregister_event(&mut self, name: String) {
        self.event_list.remove(&TcpServerEvent::Token(name));
    }

    pub fn accept(&self) -> Option<(TcpClient, String)> {
        let mut client_addr: SOCKADDR_IN = unsafe { mem::zeroed() };
        let mut sin_size: i32 = mem::size_of::<SOCKADDR_IN>() as i32;
        let client_socket: SOCKET = unsafe {
            accept(
                self.socket,
                mem::transmute::<&mut SOCKADDR_IN, *mut SOCKADDR>(&mut client_addr),
                &mut sin_size,
            )
        };
        if client_socket == INVALID_SOCKET {
            debug!("Invalid client socket: GLE={:}", unsafe {
                WSAGetLastError()
            });
            return None;
        }

        let mut buffer: Vec<u16> = Vec::with_capacity(16);

        let ret = unsafe {
            InetNtopW(
                client_addr.sin_family as INT,
                mem::transmute::<&mut in_addr, LPVOID>(&mut client_addr.sin_addr),
                buffer.as_mut_ptr(),
                16,
            )
        };
        if ret == 0 as PCWSTR {
            debug!(
                "Failed to convert client IP addr to string: GLE={:}",
                unsafe { WSAGetLastError() }
            );
            return None;
        }

        let client_addr_pair = unsafe {
            let size = libc::wcslen(ret);
            format!(
                "{}:{}",
                WideString::from_ptr(ret, size).to_string_lossy(),
                client_addr.sin_port
            )
        };

        Some((TcpClient::from_accept(client_socket), client_addr_pair))
    }

    pub fn get_event(&self) -> TcpServerEvent {
        let mut events: Vec<HANDLE> = Vec::with_capacity(self.event_list.len());
        let mut keys: Vec<&TcpServerEvent> = Vec::with_capacity(self.event_list.len());
        for (key, handle) in &self.event_list {
            keys.push(key);
            events.push(handle.clone());
        }

        // TODO: This will only allow 64 events...
        let ret = unsafe {
            WSAWaitForMultipleEvents(
                events.len() as u32,
                events.as_ptr(),
                0,
                0xffffffff as DWORD,
                0,
            )
        };
        let index = ret as usize;
        if index >= keys.len() {
            TcpServerEvent::Error
        } else {
            keys[index].clone()
        }
    }
}

impl Drop for TcpServer {
    fn drop(&mut self) {
        unsafe {
            closesocket(self.socket);
            WSACleanup();
        }
    }
}

impl HasRawHandle for TcpServer {
    fn raw_handle(&self) -> HANDLE {
        self.socket as HANDLE
    }
}
