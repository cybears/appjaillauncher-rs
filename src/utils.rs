use std::io::{Error, ErrorKind};
use std::path::Path;
use winapi::shared::ntdef::{HANDLE, NULL};
use winapi::um::handleapi::{CloseHandle, INVALID_HANDLE_VALUE};
use winapi::um::processthreadsapi::TerminateProcess;
use winapi::um::synchapi::WaitForSingleObject;
use winapi::um::winbase::INFINITE;
use winapi::um::winnt::PSID;
use windows_acl::acl::{AceType, ACL};
use windows_acl::helper::string_to_sid;

pub struct HandlePtr {
    pub raw: HANDLE,
}

impl HandlePtr {
    pub fn new(ptr: HANDLE) -> HandlePtr {
        HandlePtr { raw: ptr }
    }
}

impl Drop for HandlePtr {
    fn drop(&mut self) {
        debug!("Closing handle {:?}", self.raw);
        if self.raw != NULL && self.raw != INVALID_HANDLE_VALUE {
            unsafe { CloseHandle(self.raw) };
            self.raw = INVALID_HANDLE_VALUE;
        }
    }
}

pub struct WindowsProcess {
    pub handle: HandlePtr,
    pub pid: u32,
    pub stdin: HandlePtr,
    pub stdout: HandlePtr,
    pub extra_handles: Vec<HandlePtr>,
}

impl WindowsProcess {
    pub fn new(handle: HandlePtr, pid: u32, stdin: HandlePtr, stdout: HandlePtr) -> WindowsProcess {
        WindowsProcess {
            handle,
            pid,
            stdin,
            stdout,
            extra_handles: Vec::new(),
        }
    }

    pub fn terminate(&mut self, wait: bool) {
        debug!("Terminating process with handle {:?}", self.handle.raw);
        unsafe {
            TerminateProcess(self.handle.raw, 0xffffffff);
        }
        if wait {
            unsafe {
                WaitForSingleObject(self.handle.raw, INFINITE);
            }
        }
    }
}

impl Drop for WindowsProcess {
    fn drop(&mut self) {
        self.terminate(false);
    }
}

pub struct AclEntry {
    path: String,
    sid: String,
}

impl AclEntry {
    pub fn new(path: &Path, string_sid: &str, mask: u32, inherit: bool) -> Result<AclEntry, Error> {
        let string_path = path.to_str().unwrap_or("");
        if string_path.is_empty() {
            return Err(Error::new(
                ErrorKind::Other,
                format!("Path contains invalid characters: path={:?}", path),
            ));
        }

        let mut acl = ACL::from_file_path(string_path, false).map_err(|code| {
            Error::new(
                ErrorKind::Other,
                format!(
                    "Failed to get ACL from {:?} while adding ACL entry: error={}",
                    path, code
                ),
            )
        })?;

        let sid = string_to_sid(string_sid).unwrap_or(Vec::new());
        if sid.capacity() == 0 {
            return Err(Error::new(
                ErrorKind::Other,
                format!("Failed to convert string SID to SID: sid={:?}", string_sid),
            ));
        }

        acl.remove(
            sid.as_ptr() as PSID,
            Some(AceType::AccessAllow),
            Some(inherit),
        )
        .map_err(|code| {
            Error::new(
                ErrorKind::Other,
                format!(
                    "Failed to remove existing entry for sid={:?}: error={}",
                    string_sid, code
                ),
            )
        })?;

        acl.allow(sid.as_ptr() as PSID, inherit, mask)
            .map_err(|code| {
                Error::new(
                    ErrorKind::Other,
                    format!("Failed to add access allowed entry: error={}", code),
                )
            })?;

        Ok(AclEntry {
            path: string_path.to_string(),
            sid: string_sid.to_string(),
        })
    }

    fn remove(&self) -> bool {
        match ACL::from_file_path(&self.path, false) {
            Ok(mut acl) => {
                let sid = string_to_sid(&self.sid).unwrap_or(Vec::new());
                if sid.capacity() == 0 {
                    error!("Failed to convert string SID into SID: sid={:?}", self.sid);
                    return false;
                }

                let result = acl.remove(
                    sid.as_ptr() as PSID,
                    Some(AceType::AccessAllow),
                    Some(false), // Even if we created an inherit ACE, we want to remove both the actual and inherit ACE so have to pass false
                );
                if result.is_err() {
                    error!(
                        "Failed to remove ACL for sid={:?}: error={}",
                        self.sid,
                        result.unwrap_err()
                    );
                    return false;
                }
            }
            Err(code) => {
                error!(
                    "Failed to get ACL from path while removing ACL entry: path={:?}, error={}",
                    self.path, code
                );
                return false;
            }
        }
        true
    }
}

impl Drop for AclEntry {
    fn drop(&mut self) {
        debug!("Removing ACL entry for {:?}", self.path);
        self.remove();
    }
}
