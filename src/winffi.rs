#![cfg(windows)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]

use libloading::{Library, Symbol};
#[allow(unused_imports)]
use log::*;
use winapi::shared::basetsd::*;
use winapi::shared::minwindef::*;
use winapi::um::winnt::*;

const PROC_THREAD_ATTRIBUTE_NUMBER: DWORD = 0x0000ffff;
const PROC_THREAD_ATTRIBUTE_INPUT: DWORD = 0x00020000;
const ProcThreadAttributeSecurityCapabilities: DWORD = 9;
pub const PROC_THREAD_ATTRIBUTE_SECURITY_CAPABILITIES: SIZE_T =
    ((ProcThreadAttributeSecurityCapabilities & PROC_THREAD_ATTRIBUTE_NUMBER)
        | PROC_THREAD_ATTRIBUTE_INPUT) as SIZE_T;

lazy_static! {
    static ref PROFEXT_LIB: Library = Library::new("profext.dll").unwrap();
    static ref CREATE_APP_CONTAIER_PROFILE_WORKER_FUNC: Symbol<
        'static,
        unsafe extern "stdcall" fn(
            PCWSTR,
            PCWSTR,
            PCWSTR,
            PSID_AND_ATTRIBUTES,
            DWORD,
            DWORD,
            *mut PSID,
        ) -> HRESULT,
    > = unsafe {
        PROFEXT_LIB
            .get(b"CreateAppContainerProfileWorker\0")
            .unwrap()
    };
    static ref DELETE_APP_CONTAIER_PROFILE_WORKER_FUNC: Symbol<'static, unsafe extern "stdcall" fn(PCWSTR, DWORD) -> HRESULT> = unsafe {
        PROFEXT_LIB
            .get(b"DeleteAppContainerProfileWorker\0")
            .unwrap()
    };
}

pub unsafe fn CreateAppContainerProfileWorker(
    pszAppContainerName: PCWSTR,
    pszDisplayName: PCWSTR,
    pszDescription: PCWSTR,
    pCapabilities: PSID_AND_ATTRIBUTES,
    dwCapabilityCount: DWORD,
    r#type: DWORD,
    ppSidAppContainerSid: *mut PSID,
) -> HRESULT {
    CREATE_APP_CONTAIER_PROFILE_WORKER_FUNC(
        pszAppContainerName,
        pszDisplayName,
        pszDescription,
        pCapabilities,
        dwCapabilityCount,
        r#type,
        ppSidAppContainerSid,
    )
}

pub unsafe fn DeleteAppContainerProfileWorker(
    pszAppContainerName: PCWSTR,
    r#type: DWORD,
) -> HRESULT {
    DELETE_APP_CONTAIER_PROFILE_WORKER_FUNC(pszAppContainerName, r#type)
}
