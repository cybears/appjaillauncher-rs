#![cfg(windows)]

extern crate clap;
extern crate env_logger;
extern crate field_offset;
extern crate libc;

extern crate libloading;
#[macro_use]
extern crate log;
#[macro_use]
extern crate lazy_static;
extern crate filetime;
extern crate widestring;
extern crate winapi;
extern crate windows_acl;

include!(concat!(env!("OUT_DIR"), "/version.rs"));

mod appcontainer;
mod asw;
mod instance;
mod utils;
mod winffi;

use instance::start_instance;

use asw::HasRawHandle;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use std::process;
use utils::HandlePtr;

use clap::{App, Arg, ArgMatches, SubCommand};
#[allow(unused_imports)]
use log::*;

fn build_version() -> String {
    let prebuilt_ver = semver();
    if prebuilt_ver.len() == 0 {
        return format!("build-{} ({})", short_sha(), short_now());
    }

    format!("{}", prebuilt_ver)
}

// TODO: Handle ctrl+c and cleanup
fn do_run(matches: &ArgMatches) -> bool {
    let key_path = PathBuf::from(matches.value_of("key").unwrap());
    info!("  key_path = {:?}", key_path);

    if !key_path.exists() || key_path.is_dir() || !key_path.is_file() {
        error!("Specified key path ({:?}) is invalid", key_path);
        return false;
    }

    let mut key_dir_path = key_path.clone();
    key_dir_path.pop();

    let child_path = Path::new(matches.value_of("CHILD_PATH").unwrap());
    info!("  child_path = {:?}", child_path);

    if !child_path.exists() || child_path.is_dir() || !child_path.is_file() {
        error!("Specified child path ({:?}) is invalid", child_path);
        return false;
    }

    let port = matches.value_of("port").unwrap();
    info!("  tcp server port = {:}", port);

    let profile_name = matches.value_of("name").unwrap();
    info!("  profile name = {:}", profile_name);

    let mut instances = HashMap::new();

    info!("Attempting to bind to port {:}", port);
    let mut server = match asw::TcpServer::bind(port) {
        Ok(x) => x,
        Err(x) => {
            error!("Failed to bind server socket on port {:}: GLE={:}", port, x);
            return false;
        }
    };

    println!("Listening for clients on port {:}", port);

    // TODO: maybe make it timeout after a certain number of seconds (make it a param) so we can check if processes should die
    let mut instance_count: usize = 0;
    loop {
        match server.get_event() {
            asw::TcpServerEvent::Accept => {
                let raw_client = server.accept();
                if raw_client.is_some() {
                    let (client, addr) = raw_client.unwrap();
                    let socket_in = HandlePtr::new(client.raw_handle());
                    let socket_out = HandlePtr::new(client.raw_handle());

                    let instance_profile_name = format!("{}.{}", profile_name, instance_count);
                    instance_count += 1;
                    info!(
                        "Starting instance with profile name {}",
                        instance_profile_name
                    );

                    match start_instance(
                        &instance_profile_name,
                        matches.is_present("outbound"),
                        matches.is_present("debug"),
                        &key_path,
                        &child_path,
                        socket_in,
                        socket_out,
                    ) {
                        Ok(x) => {
                            println!(" + Accepted new client connection from {:}", addr);
                            server.register_event(
                                instance_profile_name.clone(),
                                x.process.handle.raw,
                            );
                            instances.insert(instance_profile_name, x);
                        }
                        Err(x) => {
                            error!("{:}", x);
                        }
                    }
                }
            }
            asw::TcpServerEvent::Token(name) => {
                info!("Child process for {} died, dropping instance", name);
                instances.remove(&name);
                server.deregister_event(name);
            }
            asw::TcpServerEvent::Error => {
                info!("Wait failed, stopping event loop");
                return false;
            }
        }
    }
}

fn main() {
    let app_version: &str = &build_version();
    let matches = App::new("AppJailLauncher")
        .version(app_version)
        .author("Andy Ying <andy@trailofbits.com>")
        .about("A TCP server meant for spawning AppContainer'd client processes for Windows-based CTF challenges")
        .subcommand(SubCommand::with_name("run")
            .version(app_version)
            .about("Launch a TCP server")
            .arg(Arg::with_name("name")
                     .short("n")
                     .long("name")
                     .value_name("NAME")
                     .default_value("default.appjail.profile")
                     .help("AppContainer profile name"))
            .arg(Arg::with_name("debug")
                     .long("debug")
                     .help("Enable debug mode where the AppContainers are disabled"))
            .arg(Arg::with_name("outbound")
                     .long("enable-outbound")
                     .help("Enables outbound network connections from the AppContainer'd process"))
            .arg(Arg::with_name("key")
                     .short("k")
                     .long("key")
                     .value_name("KEYFILE")
                     .required(true)
                     .help("The path to the \"key\" file that contains the challenge solution token"))
            .arg(Arg::with_name("port")
                     .short("p")
                     .long("port")
                     .value_name("PORT")
                     .default_value("4444")
                     .help("Port to bind the TCP server on"))
            .arg(Arg::with_name("CHILD_PATH")
                     .index(1)
                     .required(true)
                     .help("Path to the child process to be AppContainer'd upon TCP client acceptance")))
        .get_matches();

    env_logger::init();

    if let Some(run_matches) = matches.subcommand_matches("run") {
        info!("Detected subcommand 'run'");
        if !do_run(run_matches) {
            process::exit(1);
        }
    } else {
        error!("No subcommand provided!");
        process::exit(1);
    }
}
